# MoodleNet: Connecting and empowering educators worldwide
# Copyright © 2018-2020 Moodle Pty Ltd <https://moodle.com/moodlenet/>
# SPDX-License-Identifier: AGPL-3.0-only
defmodule MoodleNet.Uploads.Upload do
  use MoodleNet.Common.Schema
  import MoodleNet.Common.Changeset, only: [change_public: 1]

  alias Ecto.Changeset
  alias MoodleNet.Meta.Pointer
  alias MoodleNet.Users.User

  @type t :: %__MODULE__{}

  table_schema "mn_upload" do
    # has_one(:preview, __MODULE__)
    belongs_to(:parent, Pointer)
    belongs_to(:uploader, User)
    field(:path, :string)
    # FIXME
    field(:url, :string, virtual: true)
    field(:size, :integer)
    field(:media_type, :string)
    field(:metadata, :map)
    field(:is_public, :boolean, virtual: true)
    field(:published_at, :utc_datetime_usec)
    field(:deleted_at, :utc_datetime_usec)
    timestamps(inserted_at: :created_at)
  end

  @create_cast ~w(path size media_type metadata is_public)a
  @create_required ~w(path size media_type)a

  def create_changeset(parent, %User{} = uploader, attrs) do
    %__MODULE__{}
    |> Changeset.cast(attrs, @create_cast)
    |> Changeset.validate_required(@create_required)
    |> Changeset.change(
      is_public: true,
      parent_id: parent.id,
      uploader_id: uploader.id
    )
    |> change_public()
  end

  def soft_delete_changeset(%__MODULE__{} = upload) do
    MoodleNet.Common.Changeset.soft_delete_changeset(upload)
  end
end
